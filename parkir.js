//inisialisasi variabel
let parkingLot = []; //array untuk menyimpan kendaraan di tempat parkir
let maxSlots = 0; //variabel untuk menyimpan jumlah maksimum tempat parkir

//fungsi untuk membuat tempat parkir
function createParkingLot(n) {
  maxSlots = n;
  for (let i = 0; i < n; i++) {
    parkingLot.push(null);
  }
  console.log(`Created a parking lot with ${n} slots`);
}

//fungsi untuk mencari slot parkir kosong
function findEmptySlot() {
  for (let i = 0; i < maxSlots; i++) {
    if (parkingLot[i] === null) {
      return i;
    }
  }
  return -1; //jika tidak ditemukan slot kosong
}

//fungsi untuk memarkir kendaraan
function parkCar(regNo, color) {
  const emptySlot = findEmptySlot();
  if (emptySlot === -1) {
    console.log("Sorry, parking lot is full");
  } else {
    parkingLot[emptySlot] = {
      regNo: regNo,
      color: color,
    };
    console.log(`Allocated slot number: ${emptySlot + 1}`);
  }
}

//fungsi untuk mengeluarkan kendaraan dari tempat parkir
function leaveParking(slot) {
  if (parkingLot[slot - 1] === null) {
    console.log(`Slot number ${slot} is already empty`);
  } else {
    parkingLot[slot - 1] = null;
    console.log(`Slot number ${slot} is free`);
  }
}

//fungsi untuk menampilkan status tempat parkir
function checkStatus() {
  console.log("Slot No.\tRegistration No\tColour");
  for (let i = 0; i < maxSlots; i++) {
    if (parkingLot[i] !== null) {
      console.log(`${i + 1}\t${parkingLot[i].regNo}\t${parkingLot[i].color}`);
    } else {
      console.log(`${i + 1}\t - \t -   `);
    }
  }
}

//contoh penggunaan fungsi
createParkingLot(4);
parkCar("G-1234-FH", "White");
parkCar("A-2334-CT", "White");
parkCar("L-4484-AT", "Black");
parkCar("A-9524-GD", "Red");
parkCar("N-1823-JV", "Blue");
leaveParking(2);
checkStatus();
